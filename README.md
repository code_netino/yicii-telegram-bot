# YiCii-Telegram-bot


YiCii bot is basically a telegram chatbot that manages all the queries relating to the Young Indians CII club of Rajalakshmi Engineering college.

  - Currently live at https://web.telegram.org/#/im?p=@YiRecbot
  - Completely Automates the work done by the HR and the promotions team.
  




### Tech

YiCii bot uses the following frameworks

* [Tensorflow](https://www.tensorflow.org/) - Used for building the deep learning model to give the predicted response based upon training
* [NLTK](https://www.nltk.org/) - Natural language toolkit, used for the text processing operations.
* [BotFather](https://telegram.me/BotFather) - Telegram API used for intefacing the backend of the bot with Telegram messaging service through the generated API token.



### Deployment

Deployed through the Heroku cloud application platform.

### Current useage

Currently used by all members of the club for queries regarding the club activities and events.







